package com.company;

import java.io.*;
import java.util.Comparator;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        TreeSet<String> mySet = new TreeSet<>(new MyComparator()); // множество для хранения имен файлов

        while (true) {
            String sourceFileName = reader.readLine();          // читаем строку
            if (sourceFileName.equals("end")) break;            // если считали "end" то прекращаем считывать
            mySet.add(sourceFileName);                          // заносим считаное имя файла в множество
        }
        reader.close();

        String temp = mySet.first();
        String destinationFileName = temp.substring(0, temp.lastIndexOf(".")); // остается только нужная по условию часть "C:\Lion.avi"

        BufferedOutputStream buffOut = new BufferedOutputStream(new FileOutputStream(destinationFileName), 1024);
        for (String fileName : mySet) {
            BufferedInputStream buffIn = new BufferedInputStream(new FileInputStream(fileName), 1024);
            while (buffIn.available() > 0) {
                System.out.println("Read in " + fileName + ". Write to " + destinationFileName);
                int tmp = buffIn.read();                                              // читаем байт
                buffOut.write(tmp);                                                    // записываем байт
            }
            buffIn.close();                                                           // закрываем поток
        }
        buffOut.close();
    }

    public static class MyComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int x = Integer.parseInt(o1.substring(o1.lastIndexOf(".") + 5));
            int y = Integer.parseInt(o2.substring(o2.lastIndexOf(".") + 5));

            if (x > y) {
                return 1;
            }
            if (x < y) {
                return -1;
            } else return 0;
        }
    }
}
